# Weather Report App

Cloned from [ReduxSimpleStarter ](https://github.com/StephenGrider/ReactStarter/releases) following the course [React-Redux](https://www.udemy.com/react-redux/)

A simple application allowing the user to search weather forecasts for cities in US

### How to run
```
> npm install
> npm start
```