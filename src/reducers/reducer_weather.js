import { FETCH_WEATHER } from '../actions/index';

export default function(state=[], action){

    switch (action.type){
        case FETCH_WEATHER:
            //never manipulate state here for example
            //return state.push(action.payload.data)
            //instead we can use
            //return state.concat(action.payload.data);
            //es shortcut
            return [action.payload.data, ...state];

    }
    return state;
}